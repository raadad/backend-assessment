console.log('Booking Price Calculator.');
const fs = require('fs');
const calcLib = require('./bookingCalculator');

console.log('Reading File...');
const inputFilePath = './input.json';
const inputString = fs.readFileSync(inputFilePath).toString();
const input = JSON.parse(inputString);

console.log('Writing File...');
const output = calcLib.applyRulesToBooking(input);
fs.writeFileSync('./output.json', JSON.stringify(output, null, 2));

console.log('Done. Thank you for your consideration!');
