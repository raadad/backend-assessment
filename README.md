## Hello!
Thanks for your consideration! I had some fun putting this together

### requirements
  - a recent version of node (tested on v9.2)
  - git
### instructions
1. clone this repo
2. `npm install` - installs the required dependencies
3. `node index.js` - runs the main program
4. `./node_modules/.bin/mocha spec/**_spec.js` - executes the test suite
