const assert = require('assert');
const moment = require('moment');

const lib = require('../bookingCalculator.js');

const rules = lib.genRuleSet().reduce((acc, rule) => {
	acc[rule.name] = rule;
	return acc;
},{});


 
describe('bookingCalculator', () => {
	describe('integration tests', () => {
		it('should process the inputs as expected', () => {
			const fs  = require('fs');
			const inputString = fs.readFileSync('./input.json').toString();
			const output = lib.applyRulesToBooking(JSON.parse(inputString));
			const expectedOutputString = fs.readFileSync('./expectedOutput.json').toString();
			assert.equal(JSON.stringify(output, null, 2), JSON.stringify(JSON.parse(expectedOutputString), null, 2));
		});
	});

	describe('Default Rules', () => {
		describe('Rule - mintime', () => {
			const ruleName = 'mintime';
		    it('should exist', () => { 
		    	assert.equal(rules[ruleName].name, ruleName);
		    });

		    it('should pass for a booking that is longer than one hour', () => {
				const booking = lib.genRuleObject({
					'id': 1,
					'from': '2017-10-23T08:00:00+11:00',
					'to': '2017-10-23T11:00:00+11:00'
				});
				assert.equal(rules[ruleName].logic(booking).isValid, true);
		    });

		    it('should fail for a booking that is shorter than one hour', () => {
				const booking = lib.genRuleObject({
					'id': 1,
					'from': '2017-10-23T08:00:00+11:00',
					'to': '2017-10-23T08:30:00+11:00'
				});
				assert.equal(rules[ruleName].logic(booking).isValid, false);
		    });
		});

		describe('Rule - maxtime', () => {
			const ruleName = 'maxtime';
		    it('should exist', () => { 
		    	assert.equal(rules[ruleName].name, ruleName);
		    });

		    it('should pass for a booking that is not longer than 24 hours', () => {
				const booking = lib.genRuleObject({
					'id': 1,
					'from': '2017-10-23T08:00:00+11:00',
					'to': '2017-10-23T11:00:00+11:00'
				});
				assert.equal(rules[ruleName].logic(booking).isValid, true);
		    });

		    it('should fail for a booking that is longer than 24 hours', () => {
				const booking = lib.genRuleObject({
					'id': 1,
					'from': '2017-10-23T08:00:00+11:00',
					'to': '2017-10-24T08:30:00+11:00'
				});
				assert.equal(rules[ruleName].logic(booking).isValid, false);
		    });
		});
		describe.skip('should of included a couple of tests for each case', () => {
		    it('are tests really that important?', () => {
				assert.equal('no', 'yes!');		    	
		    });			
		});
	});
});
