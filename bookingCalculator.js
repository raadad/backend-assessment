const moment = require('moment');

const defaultConfig = {
  dayRate: 38,
  nightRate: 42.93,
  nightStart: 20,
  nightEnd: 6,
  satRate: 45.91,
  sunRate: 60.85,
  minBookingTime: 1 * 60,
  maxBookingTime: 1 * 60 * 24,
  timeIncrements: 15
};
const genRuleSet = (config = defaultConfig) => {
  return [
    {
      name: 'mintime',
      desc: 'The minimum booking time is 1 hour',
      logic: booking => {
        if (booking.minutes < config.minBookingTime) booking.isValid = false;
        return booking;
      }
    },
    {
      name: 'maxtime',
      desc: 'The maximum booking time is 24 hours',
      logic: booking => {
        if (booking.minutes > config.maxBookingTime) booking.isValid = false;
        return booking;
      }
    },
    {
      name: 'forwardtime',
      desc: 'A booking cannot end before it has started',
      logic: booking => {
        if (booking.minutes < 0) booking.isValid = false;
        return booking;
      }
    },
    {
      name: 'timeincrements',
      desc: 'A booking can be booked in 15 min increments e.g. 1600 to 1715',
      logic: booking => {
        if (!booking.minutes % config.timeIncrements) booking.isValid = false;
        return booking;
      }
    },
    {
      name: 'applyDayRate',
      desc: 'determines the cost of Hireup bookings',
      logic: booking => {
        booking.total = Number(
          (booking.minutes / 60 * config.dayRate).toFixed(2)
        );
        return booking;
      }
    },
    {
      name: 'applynightrate',
      desc:
        'If any part of a booking is charged at the night rate, the whole booking is charged at the night rate',
      logic: booking => {
        var startHour = booking.fromMoment.hour();
        var finishHour = booking.toMoment.hour();
        if (
          startHour >= config.nightStart ||
          startHour <= config.nightEnd ||
          finishHour >= config.nightStart ||
          finishHour <= config.nightEnd
        )
          booking.total = Number(
            (booking.minutes / 60 * config.nightRate).toFixed(2)
          );
        return booking;
      }
    },
    {
      name: 'applyWeekendRate',
      desc:
        'Saturday and Sunday rates apply across the whole day, there\'s no distinction between day and night:',
      logic: booking => {
        var startDay = booking.fromMoment.day();
        var finishDay = booking.toMoment.day();

        if (startDay === 6 || finishDay === 6)
          booking.total = Number(
            (booking.minutes / 60 * config.satRate).toFixed(2)
          ); //If Saturday
        if (startDay === 0 || finishDay === 0)
          booking.total = Number(
            (booking.minutes / 60 * config.sunRate).toFixed(2)
          ); //If Sunday

        return booking;
      }
    },
    {
      name: 'resetTotalIfNotValid',
      desc:
        'If the booking breaks one of the business rules, the total should be set to 0',
      logic: booking => {
        if (!booking.isValid) booking.total = 0;
        return booking;
      }
    }
  ];
};

const lib = {};

lib.genRuleObject = booking => {
  const dateFormat = 'YYYY-MM-DDTHH:mm:ssZ';
  return {
    ...booking,
    fromMoment: moment(booking.from, dateFormat),
    toMoment: moment(booking.to, dateFormat),
    isValid: true,
    minutes: moment(booking.to, dateFormat).diff(
      moment(booking.from, dateFormat),
      'minutes'
    )
  };
};

lib.applyRulesToBooking = (input, rules = genRuleSet()) => {
  return input.map(booking => {
    const { id, from, to, isValid, total } = rules.reduce((acc, rule) => {
      return rule.logic(acc);
    }, lib.genRuleObject(booking));
    return { id, from, to, isValid, total };
  });
};

lib.defaultConfig = defaultConfig;
lib.genRuleSet = genRuleSet;

module.exports = lib;
